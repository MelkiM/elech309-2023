# ELECH309

Le but du projet intégré d'électricité est de donner l'occasion aux étudiants d'utiliser des concepts vus dans différents cours de la filière électronique-télécommunications, pour mener à bien un projet multidisciplinaire de grande ampleur, ce qui est impossible dans les travaux pratiques classiquement associés à un cours.

## Objectif du projet

L'objectif technique du projet est de réaliser le système de contrôle d'un robot.  Ce robot doit pouvoir se déplacer en ligne droite et effectuer des rotations sur place.  Les déplacements qu'il doit effectuer lui seront communiqués au moyen d'un canal de communication audio.  
Le robot devra réagir à deux ordres :

* "Déplace-toi en ligne droite de X cm".
* "Effectue une rotation sur place de Y degrés".

## Organisation du projet

Le projet est divisé en 15 séances de 4h, étalées sur tout le second quadrimestre.  Il a été conçu pour une équipe de 2 ou 3 étudiants.  
Ce projet a également pour but de vous faire réfléchir sur la méthodologie à adopter pour aborder un projet d'ingénierie, vous devrez organiser vous-même votre travail.  
Ce dépôt Gitlab met à votre disposition les documents et les exemples de code utiles au projet.  

En particulier, le répertoire "Description du projet" contient les documents qui vous aideront à démarrer :

* L'analyse préliminaire du projet dans laquelle sont décrits et justifiés les choix techniques qui vous sont imposés
* L'étude du déplacement du robot qui décrit les périphériques utilisés pour interfacer le microcontrôleur aux moteurs et encodeurs du robot, ainsi que le dimensionnement de la régulation que nous vous proposons d'utiliser.
* L'étude de la communication audio qui décrit le protocole utilisé, ainsi que le démodulateur que nous vous proposons d'implémenter.

Le répertoire "Exercices" contient des séances d'introduction aux premiers périphériques que vous allez utilisez.  Ils vous aideront aussi à vous familiarisez avec le microcontrôleur utilisé dans le projet.
