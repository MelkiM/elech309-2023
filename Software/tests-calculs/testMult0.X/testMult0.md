# testMult0.X

## description

Ce projet permet de déterminer la durée d'une simple multiplication entre 2 nombres pour différents types de variables.

Pour cela, on utilise le simulateur intégré à MPLABX.  Il nous permet de tester des codes sans avoir besoin d'un µC.  
Il nous donne les temps d'exécution (exprimés en cycles-machine), en utilisant l'outil *stopwatch*.

## Résultats

### Multiplication 16x16 -> 32

* temps de calcul : 10 cycles (= 250 ns à 40 MIPS)
* Le résultat du calcul est faux, car on ne récupère que les 16 LSB du résultat de la multiplication

### Multiplication 16x16 -> 32 avec "casting" des opérandes

* temps de calcul : 20 cycles (= 500 ns à 40 MIPS)
* Le résultat est correct

### Multiplication 32x16 -> 32

* temps de calcul : 27 cycles (= 675 ns à 40 MIPS)
* Le résultat est correct

### Multiplication 32x32 -> 32

* temps de calcul : 32 cycles (= 800 ns à 40 MIPS)
* Le résultat est correct

### Multiplication floatxfloat -> float

* temps de calcul : ~85 cycles (= 2,125 ns à 40 MIPS), dépend de la valeur des opérandes
* Le résultat est correct

### Multiplication 16x16 -> 32 avec fonction "hardware-dependent"

* temps de calcul : 8 cycles (= 200 ns à 40 MIPS)
* Le résultat est correct
