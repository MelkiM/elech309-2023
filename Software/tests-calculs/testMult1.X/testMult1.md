# testMult1.X

## description
 à 40 MIPS
Ce projet permet de déterminer la durée d'une simple multiplication entre 2 nombres pour différents types de variables.

Pour cela, on utilise le simulateur intégré à MPLABX.  Il nous permet de tester des codes sans avoir besoin d'un µC.  
Il nous donne les temps d'exécution (exprimés en cycles-machine), en utilisant l'outil *stopwatch*.

## Résultats


### Multiplication 16x16 -> 32 avec "casting" des opérandes

#### niveau d'optimisation 0

* *sample = adcRead();* : 19 cycles (= 475 ns)
* *c = (int32_t)a16*sample;* : 20 cycles
* reste de la boucle : 75 cycles

#### niveau d'optimisation 1

* *sample = adcRead();* 
* *c = (int32_t)a16*sample;* : 12 cycles
* reste de la boucle : 68 cycles

### Multiplication 32x32 -> 32


### Multiplication floatxfloat -> float

* temps de calcul : ~85 cycles (= 2,125 ns à 40 MIPS), dépend de la valeur des opérandes
* Le résultat est correct
