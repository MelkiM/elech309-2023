/*
 * Main file of voltmetre2.X
 * Author: mosee
 *
 */

#include "xc.h"
#include "adc.h"

int main(void) {
    int16_t a16 = 1000, sample;
    int32_t a32, c;  
    float af, cf;
    
    a32 = 2*a16;
    af = 3*a16;
    
    TRISB = 0;
    adcInit(ADC_MANUAL_SAMPLING);
    
    while(1){
        adcStart();
        while (!adcConversionDone());
        sample = adcRead();             // opt 0        opt1
        LATB = sample;                  // 4 cycles     1 cycle
        c = (int32_t)a16*sample;        // 20 cycles    2 cycles
        LATB = (c >> 16);               // 8 cycles     1 cycle
        c = a32*sample;                 // 27 cycles    2 cycles
        LATB = (c >> 16);               // 8 cycles     1 cycle
        c = __builtin_mulss(a16,sample);// 8 cycles     2 cycles
        LATB = (c >> 16);               // 8 cycles     1 cycle
        cf = af*sample;                 // 280 cycles   275 cycles
        LATB = cf/65536;                // 324 cycles   208 cycles
    }
}
