# Guides

Ce répertoire contient des guides pratiques sur l'utilisation des outils utilisés dans le projet :

* [Installation_MPLABX.md](Installation_MPLABX.md) explique comment installer l'*IDE* et le compilateur permettant de programmer le dsPIC
* [Creation_nouveau_projet.md](Creation_nouveau_projet.md) explique comment créer un projet *MPLAB X* pour le [module MC802](../Hardware/module_MC802.md)
* [Copie_projet_existant.md](Copie_projet_existant.md) explique comment cloner un projet existant
* [Correction_erreurs.md](Correction_erreurs.md) explique comment *MPLAB X* peut aider à trouver une erreur dans un code
* [Compilation_programmation.md](Compilation_programmation.md) explique comment compiler un projet et le transférer dans le dsPIC
* [Introduction_C.md](Introduction_C.md) est un rappel des notions de base  du **C**.
